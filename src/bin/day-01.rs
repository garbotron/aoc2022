use std::fs;
use itertools::Itertools;

fn parse_elf_input(s: &str) -> Vec<i32> {
    s.lines().map(|x| x.parse().unwrap()).collect()
}

fn main() {
    let input = fs::read_to_string("input/day-01.txt").unwrap();
    let sum_per_elf = input
        .split("\n\n")
        .map(parse_elf_input)
        .map(|x| x.iter().sum::<i32>())
        .collect_vec();

    let part1_result = sum_per_elf.iter().max().unwrap();
    println!("Part 1: {part1_result}");

    let part2_result = sum_per_elf.iter().sorted().rev().take(3).sum::<i32>();
    println!("Part 2: {part2_result}");
}
