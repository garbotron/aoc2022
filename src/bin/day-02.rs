use std::fs;

#[derive(PartialEq)]
enum Shape {
    Rock,
    Paper,
    Scissors,
}

#[derive(PartialEq)]
enum Result {
    Win,
    Loss,
    Tie,
}

impl Shape {
    fn parse_abc(s: &str) -> Self {
        match s {
            "A" => Self::Rock,
            "B" => Self::Paper,
            "C" => Self::Scissors,
            _ => panic!(),
        }
    }

    fn parse_xyz(s: &str) -> Self {
        match s {
            "X" => Self::Rock,
            "Y" => Self::Paper,
            "Z" => Self::Scissors,
            _ => panic!(),
        }
    }

    fn shape_score(&self) -> i32 {
        match self {
            Self::Rock => 1,
            Self::Paper => 2,
            Self::Scissors => 3,
        }
    }

    fn match_result(&self, other: &Shape) -> Result {
        match (self, other) {
            (Self::Rock, Self::Rock) => Result::Tie,
            (Self::Rock, Self::Paper) => Result::Loss,
            (Self::Rock, Self::Scissors) => Result::Win,
            (Self::Paper, Self::Rock) => Result::Win,
            (Self::Paper, Self::Paper) => Result::Tie,
            (Self::Paper, Self::Scissors) => Result::Loss,
            (Self::Scissors, Self::Rock) => Result::Loss,
            (Self::Scissors, Self::Paper) => Result::Win,
            (Self::Scissors, Self::Scissors) => Result::Tie,
        }
    }

    fn match_score(&self, other: &Shape) -> i32 {
        self.shape_score() + self.match_result(other).score()
    }
}

impl Result {
    fn score(&self) -> i32 {
        match self {
            Self::Win => 6,
            Self::Loss => 0,
            Self::Tie => 3,
        }
    }

    fn parse_xyz(s: &str) -> Self {
        match s {
            "X" => Self::Loss,
            "Y" => Self::Tie,
            "Z" => Self::Win,
            _ => panic!(),
        }
    }
}

fn main() {
    let input = fs::read_to_string("input/day-02.txt").unwrap();

    let mut part1_score = 0;
    for line in input.lines() {
        let (opponent, player) = line.split_once(' ').unwrap();
        part1_score += Shape::parse_xyz(player).match_score(&Shape::parse_abc(opponent));
    }
    println!("Part 1: {part1_score}");

    let mut part2_score = 0;
    for line in input.lines() {
        let (opponent, result) = line.split_once(' ').unwrap();
        let (opponent, result) = (Shape::parse_abc(opponent), Result::parse_xyz(result));
        let my_play = [Shape::Rock, Shape::Paper, Shape::Scissors]
            .iter()
            .find(|x| x.match_result(&opponent) == result)
            .unwrap();
        part2_score += my_play.match_score(&opponent);
    }
    println!("Part 2: {part2_score}");
}
