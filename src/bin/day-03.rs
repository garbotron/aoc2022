use std::{fs, collections::HashSet};

use itertools::Itertools;

fn priority(c: char) -> u8 {
    if c >= 'a' {
        ((c as u8) - ('a' as u8)) + 1
    } else {
        ((c as u8) - ('A' as u8)) + 27
    }
}

fn main() {
    let input = fs::read_to_string("input/day-03.txt").unwrap();

    let mut p1_total = 0;
    for line in input.lines() {
        let x: HashSet<_> = line.chars().take(line.len() / 2).collect();
        let y: HashSet<_> = line.chars().skip(line.len() / 2).collect();
        for &a in x.intersection(&y) {
            p1_total += priority(a) as i32;
        }
    }
    println!("Part 1: {p1_total}");

    let mut p2_total = 0;
    for group in &input.lines().chunks(3) {
        let group = group.collect_vec();
        let c = ('A'..='z').find(|&c| group.iter().all(|s| s.contains(c))).unwrap();
        p2_total += priority(c) as i32;
    }
    println!("Part 2: {p2_total}");
}
