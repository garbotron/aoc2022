use std::{fs, collections::HashSet};

fn parse_range(s: &str) -> HashSet<i32> {
    let (a, b) = s.split_once('-').unwrap();
    let (a, b) = (a.parse::<i32>().unwrap(), b.parse::<i32>().unwrap());
    (a..=b).collect()
}

fn parse_line(s: &str) -> (HashSet<i32>, HashSet<i32>) {
    let (a, b) = s.split_once(',').unwrap();
    (parse_range(a), parse_range(b))
}

fn main() {
    let input = fs::read_to_string("input/day-04.txt").unwrap();

    let mut p1_count = 0;
    for line in input.lines() {
        let (a, b) = parse_line(line);
        if a.is_subset(&b) || b.is_subset(&a) {
            p1_count += 1;
        }
    }
    println!("Part 1: {p1_count}");

    let mut p2_count = 0;
    for line in input.lines() {
        let (a, b) = parse_line(line);
        if a.intersection(&b).next().is_some() {
            p2_count += 1;
        }
    }
    println!("Part 2: {p2_count}");
}
