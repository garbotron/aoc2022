use std::fs;

fn main() {
    let input = fs::read_to_string("input/day-05.txt").unwrap();
    let (crate_str, procedure_str) = input.split_once("\n\n").unwrap();
    let crate_lines: Vec<_> = crate_str.lines().collect();

    let mut orig_stacks = Vec::new();
    for (i, c) in crate_lines[crate_lines.len() - 1].chars().enumerate() {
        if c != ' ' {
            let mut stack = Vec::new();
            for line in crate_lines.iter().rev().skip(1) {
                let c = line.chars().nth(i).unwrap_or(' ');
                if c != ' ' {
                    stack.push(c);
                }
            }
            orig_stacks.push(stack);
        }
    }

    let parse_procedure_line = |line: &str| {
        let words: Vec<_> = line.split(' ').collect();
        (words[1].parse::<usize>().unwrap(),
         words[3].parse::<usize>().unwrap() - 1,
         words[5].parse::<usize>().unwrap() - 1)
    };

    let mut p1_stacks = orig_stacks.clone();
    for line in procedure_str.lines() {
        let (count, from, to) = parse_procedure_line(line);
        for _ in 0..count {
            let x = p1_stacks[from].pop().unwrap();
            p1_stacks[to].push(x);
        }
    }

    let p1_result: String = p1_stacks.iter().map(|x| x.last().unwrap()).collect();
    println!("Part 1: {p1_result}");

    let mut p2_stacks = orig_stacks.clone();
    for line in procedure_str.lines() {
        let (count, from, to) = parse_procedure_line(line);
        let split_at = p2_stacks[from].len() - count;
        let mut moved = p2_stacks[from].split_off(split_at);
        p2_stacks[to].append(&mut moved);
    }

    let p2_result: String = p2_stacks.iter().map(|x| x.last().unwrap()).collect();
    println!("Part 2: {p2_result}");
}
