use std::fs;

use itertools::Itertools;

fn main() {
    let input: Vec<_> = fs::read_to_string("input/day-06.txt").unwrap().chars().collect();

    let find_unique_sequence = |len| input
        .windows(len)
        .enumerate()
        .find(|x| x.1.iter().all_unique())
        .unwrap()
        .0 + len;

    println!("Part 1: {}", find_unique_sequence(4));
    println!("Part 2: {}", find_unique_sequence(14));
}
