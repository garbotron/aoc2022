use std::{collections::{HashMap, HashSet}, fs};
use itertools::Itertools;

type Path = Vec<String>;

struct FileSystem {
    files: HashMap<Path, usize>,
    dirs: HashSet<Path>,
    wd: Path,
}

impl FileSystem {
    fn new() -> Self {
        Self { files: HashMap::new(), dirs: HashSet::new(), wd: Vec::new() }
    }

    fn cd(&mut self, dir: &str) {
        match dir {
            "/" => self.wd.clear(),
            ".." => { self.wd.pop(); },
            x => self.wd.push(x.to_string()),
        }
        self.dirs.insert(self.wd.clone());
    }

    fn add_file(&mut self, name: &str, size: usize) {
        let mut path = self.wd.clone();
        path.push(name.to_string());
        self.files.insert(path, size);
    }

    fn dir_size(&self, dir: &Path) -> usize {
        self.files.iter().filter(|(x, _)| x.starts_with(dir)).map(|(_, x)| x).sum()
    }
}

fn main() {
    let mut fs = FileSystem::new();

    let input = fs::read_to_string("input/day-07.txt").unwrap();
    let mut input_lines = input.lines().peekable();
    while let Some(line) = input_lines.next() {
        let words: Vec<_> = line.split_whitespace().collect();
        assert!(words[0] == "$");
        match words[1] {
            "cd" => fs.cd(words[2]),
            "ls" => {
                while !input_lines.peek().unwrap_or(&"$").starts_with("$") {
                    let line = input_lines.next().unwrap();
                    let (size, name) = line.split_whitespace().collect_tuple().unwrap();
                    if size != "dir" {
                        fs.add_file(name, size.parse().unwrap());
                    }
                }
            }
            _ => panic!(),
        }
    }

    let all_dir_sizes = fs.dirs.iter().map(|x| fs.dir_size(x));
    let part1_result: usize = all_dir_sizes.clone().filter(|x| *x <= 100000).sum();
    println!("Part 1: {part1_result}");

    let cur_unused = 70000000 - fs.dir_size(&Vec::new());
    let needed = 30000000 - cur_unused;
    let part2_result = all_dir_sizes.clone().filter(|x| *x >= needed).min().unwrap();
    println!("Part 2: {part2_result}");
}
