use std::{collections::HashMap, fs};

fn main() {
    let input = fs::read_to_string("input/day-08.txt").unwrap();

    let mut grid = HashMap::new();
    for (y, line) in input.lines().enumerate() {
        for (x, ch) in line.chars().enumerate() {
            grid.insert((x as i32, y as i32), ch.to_string().parse::<i32>().unwrap());
        }
    }

    let grid_width = grid.keys().map(|(x, _)| x).max().unwrap() + 1;
    let grid_height = grid.keys().map(|(_, y)| y).max().unwrap() + 1;
    let tree_height = |x, y| *grid.get(&(x, y)).unwrap();

    let is_visible = |x, y| {
        let my_height = tree_height(x, y);
        (0..x).all(|x| tree_height(x, y) < my_height) // leftward
        || (x+1..grid_width).rev().all(|x| tree_height(x, y) < my_height) // rightward
        || (0..y).all(|y| tree_height(x, y) < my_height) // downward
        || (y+1..grid_height).rev().all(|y| tree_height(x, y) < my_height) // upward
    };

    println!("Part 1: {}", grid.keys().filter(|(x, y)| is_visible(*x, *y)).count());

    let scenic_score = |x, y| {
        let my_height = tree_height(x, y);

        let left = (0..x).rev().find(|&x| tree_height(x, y) >= my_height);
        let right = (x+1..grid_width).find(|&x| tree_height(x, y) >= my_height);
        let up = (0..y).rev().find(|&y| tree_height(x, y) >= my_height);
        let down = (y+1..grid_height).find(|&y| tree_height(x, y) >= my_height);

        let left_score = if let Some(t) = left { x - t } else { x };
        let right_score = if let Some(t) = right { t - x } else { (grid_width - 1) - x };
        let up_score = if let Some(t) = up { y - t } else { y };
        let down_score = if let Some(t) = down { t - y } else { (grid_height - 1) - y };

        left_score * right_score * up_score * down_score
    };

    println!("Part 2: {}", grid.keys().map(|(x, y)| scenic_score(*x, *y)).max().unwrap());
}
