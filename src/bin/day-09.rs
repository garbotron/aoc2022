use std::{collections::HashSet, fs};
use itertools::Itertools;


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
struct Pt { x: i32, y: i32 }

#[derive(Clone, Debug)]
struct Rope { knots: Vec<Pt> }

fn add_pt(a: Pt, b: Pt) -> Pt {
    Pt { x: a.x + b.x, y: a.y + b.y }
}

fn move_tail(head: Pt, tail: Pt) -> Pt {
    let mut tail = tail;
    if (head.x - tail.x).abs() > 1 || (head.y - tail.y).abs() > 1 {
        if head.x != tail.x {
            tail.x = if tail.x < head.x { tail.x + 1 } else { tail.x - 1 };
        }
        if head.y != tail.y {
            tail.y = if tail.y < head.y { tail.y + 1 } else { tail.y - 1 };
        }
    }
    tail
}

fn move_rope(rope: &Rope, head_move: Pt) -> Rope {
    let mut knots = vec![add_pt(rope.knots[0], head_move)];
    for i in 1..rope.knots.len() {
        knots.push(move_tail(knots[i - 1], rope.knots[i]));
    }
    Rope { knots }
}

fn main() {
    let input = fs::read_to_string("input/day-09.txt").unwrap();

    let mut movement_instructions = Vec::new();
    for line in input.lines() {
        let (dir, num) = line.split_whitespace().collect_tuple().unwrap();
        let delta = match dir {
            "L" => Pt { x: -1, y: 0 },
            "R" => Pt { x: 1, y: 0 },
            "U" => Pt { x: 0, y: -1 },
            "D" => Pt { x: 0, y: 1 },
            _ => panic!(),
        };
        for _ in 0..num.parse::<i32>().unwrap() {
            movement_instructions.push(delta);
        }
    }

    let count_tail_positions = |knot_count| {
        let mut tail_positions = HashSet::new();
        let mut rope = Rope { knots: vec![Pt { x: 0, y: 0 }; knot_count] };
        for &instr in movement_instructions.iter() {
            rope = move_rope(&rope, instr);
            tail_positions.insert(rope.knots[knot_count - 1]);
        }
        tail_positions.len()
    };

    println!("Part 1: {}", count_tail_positions(2));
    println!("Part 2: {}", count_tail_positions(10));
}
