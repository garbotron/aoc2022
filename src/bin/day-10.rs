use std::fs;

#[derive(Clone, Copy)]
enum Instruction {
    Noop,
    Add(i32),
}

struct Cpu {
    program: Vec<Instruction>,
    cycle: i32,
    pc: usize,
    x: i32,
    pending_x: Option<i32>,
}

impl Instruction {
    fn parse(s: &str) -> Self {
        let words: Vec<_> = s.split_whitespace().collect();
        match words[0] {
            "noop" => Self::Noop,
            "addx" => Self::Add(words[1].parse().unwrap()),
            _ => panic!(),
        }
    }
}

impl Cpu {
    fn new(program: &Vec<Instruction>) -> Self {
        Self { program: program.clone(), cycle: 0, pc: 0, x: 1, pending_x: None }
    }

    fn run_cycle(&mut self) {
        self.cycle += 1;
        if let Some(x) = self.pending_x {
            self.x += x;
            self.pending_x = None;
        } else {
            match self.program.get(self.pc) {
                None => (),
                Some(Instruction::Noop) => (),
                Some(Instruction::Add(x)) => self.pending_x = Some(*x),
            }
            self.pc += 1;
        }
    }

    fn run_cycles(&mut self, count: usize) {
        for _ in 0..count { self.run_cycle() }
    }

    fn signal_strength(&self) -> i32 {
        (self.cycle + 1) * self.x
    }

    fn is_pixel_lit(&self) -> bool {
        let scan_x = self.cycle % 40;
        (self.x - scan_x).abs() <= 1
    }
}

fn main() {
    let input = fs::read_to_string("input/day-10.txt").unwrap();
    let instructions: Vec<_> = input.lines().map(Instruction::parse).collect();

    let mut cpu = Cpu::new(&instructions);
    let mut signal_strength = 0;
    cpu.run_cycles(19);
    for _ in 0..6 {
        signal_strength += cpu.signal_strength();
        cpu.run_cycles(40);
    }
    println!("Part 1: {}", signal_strength);

    println!("Part 2:");
    let mut cpu = Cpu::new(&instructions);
    for _ in 0..6 {
        for _ in 0..40 {
            if cpu.is_pixel_lit() { print!("#") } else { print!(".") }
            cpu.run_cycle();
        }
        println!();
    }
}
