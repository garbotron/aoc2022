use std::collections::VecDeque;

struct Monkey<'a> {
    starting_items: &'a [u64],
    operation: &'a dyn Fn(u64) -> u64,
    test: u64,
    if_true: usize,
    if_false: usize,
}

#[derive(Clone)]
struct MonkeyState<'a> {
    monkey: &'a Monkey<'a>,
    items: VecDeque<u64>,
    inspection_count: u64,
}

impl<'a> MonkeyState<'a> {
    fn new(monkey: &'a Monkey<'a>) -> Self {
        Self {
            monkey,
            items: monkey.starting_items.iter().map(|x| *x).collect(),
            inspection_count: 0,
        }
    }
}

fn perform_turn(monkeys: &mut Vec<MonkeyState>, i: usize, divisor: u64, modulo: u64) {
    while let Some(item) = monkeys[i].items.pop_front() {
        monkeys[i].inspection_count += 1;
        let item = (monkeys[i].monkey.operation)(item);
        let item = (item / divisor) % modulo;
        let dest_idx = if item % monkeys[i].monkey.test == 0 {
            monkeys[i].monkey.if_true
        } else {
            monkeys[i].monkey.if_false
        };
        monkeys[dest_idx].items.push_back(item);
    }
}

fn perform_round(monkeys: &mut Vec<MonkeyState>, divisor: u64) {
    // The key to part 2 is that we only test for divisibility.
    // So we can remain within the modulo of the LCM of all of the test vaules.
    // Or, to be lazy, the product of them.
    let modulo = monkeys.iter().map(|x| x.monkey.test).product();
    for i in 0..monkeys.len() {
        perform_turn(monkeys, i, divisor, modulo);
    }
}

fn main() {
    // No input text parsing for today... it's way too complicated.
    let input = [
        Monkey {
            starting_items: &[74, 64, 74, 63, 53],
            operation: &|x| x * 7,
            test: 5,
            if_true: 1,
            if_false: 6,
        },
        Monkey {
            starting_items: &[69, 99, 95, 62],
            operation: &|x| x * x,
            test: 17,
            if_true: 2,
            if_false: 5,
        },
        Monkey {
            starting_items: &[59, 81],
            operation: &|x| x + 8,
            test: 7,
            if_true: 4,
            if_false: 3,
        },
        Monkey {
            starting_items: &[50, 67, 63, 57, 63, 83, 97],
            operation: &|x| x + 4,
            test: 13,
            if_true: 0,
            if_false: 7,
        },
        Monkey {
            starting_items: &[61, 94, 85, 52, 81, 90, 94, 70],
            operation: &|x| x + 3,
            test: 19,
            if_true: 7,
            if_false: 3,
        },
        Monkey {
            starting_items: &[69],
            operation: &|x| x + 5,
            test: 3,
            if_true: 4,
            if_false: 2,
        },
        Monkey {
            starting_items: &[54, 55, 58],
            operation: &|x| x + 7,
            test: 11,
            if_true: 1,
            if_false: 5,
        },
        Monkey {
            starting_items: &[79, 51, 83, 88, 93, 76],
            operation: &|x| x * 3,
            test: 2,
            if_true: 0,
            if_false: 6,
        },
    ];

    let monkey_business = |round_count, divisor| {
        let mut state: Vec<_> = input.iter().map(MonkeyState::new).collect();
        for _ in 0..round_count {
            perform_round(&mut state, divisor);
        }
        state.sort_by_key(|x| !x.inspection_count);
        state[0].inspection_count * state[1].inspection_count
    };

    println!("Part 1: {}", monkey_business(20, 3));
    println!("Part 2: {}", monkey_business(10000, 1));
}
