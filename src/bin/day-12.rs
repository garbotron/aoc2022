use std::fs;

fn main() {
    let input = fs::read_to_string("input/day-12.txt").unwrap();

    // Parse into a flat vector for speed of access (rust hashmaps of tuples can be slow).
    let mut elevations = Vec::new();
    let mut start = usize::MAX;
    let mut end = usize::MAX;
    let mut width = usize::MAX;
    for line in input.lines() {
        width = line.len();
        for ch in line.chars() {
            match ch {
                'S' => { start = elevations.len(); elevations.push(0); }
                'E' => { end = elevations.len(); elevations.push(25); }
                ch => { elevations.push((ch as u8) - ('a' as u8)); }
            }
        }
    }

    // Neighbor calculation using a flat mapping is annoying.
    let neighbors = |i| [
        if i % width == 0 { None } else { Some(i - 1) },
        if i % width == width - 1 { None } else { Some(i + 1) },
        if i < width { None } else { Some(i - width) },
        if i + width >= elevations.len() { None } else { Some(i + width) },
    ];

    // Simple Dijkstra-style traversal starting at the target and working backward.
    let mut to_explore = vec![end];
    let mut costs = vec![i32::MAX; elevations.len()];
    costs[end] = 0;
    while let Some(i) = to_explore.pop() {
        for n in neighbors(i).iter().filter_map(|&x| x) {
            if elevations[n] + 1 >= elevations[i] && costs[n] > costs[i] + 1 {
                costs[n] = costs[i] + 1;
                to_explore.push(n);
            }
        }
    }

    println!("Part 1: {}", costs[start]);
    println!("Part 2: {}", (0..elevations.len()).filter(|&i| elevations[i] == 0).map(|i| costs[i]).min().unwrap());
}
