use std::{fs, cmp::Ordering};

#[derive(Debug, Clone, Eq, PartialEq)]
enum Data {
    Int(u8),
    List(Vec<Data>),
}

impl Data {
    fn parse(s: &str) -> (Data, &str) {
        if s.starts_with('[') {
            let mut list = Vec::new();
            let mut s = &s[1..];
            while !s.starts_with(']') {
                let (elem, rest) = Data::parse(s);
                list.push(elem);
                s = rest;
                if s.starts_with(',') {
                    s = &s[1..];
                }
            }
            (Data::List(list), &s[1..])
        } else {
            let num_str: String = s.chars().take_while(|c| char::is_digit(*c, 10)).collect();
            (Data::Int(num_str.parse().unwrap()), &s[num_str.len()..])
        }
    }
}

impl Ord for Data {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Data::Int(a), Data::Int(b)) => a.cmp(b),
            (Data::List(a), Data::List(b)) => match (a.first(), b.first()) {
                (None, None) => Ordering::Equal,
                (None, _) => Ordering::Less,
                (_, None) => Ordering::Greater,
                (Some(a_head), Some(b_head)) => match a_head.cmp(b_head) {
                    Ordering::Equal => Data::List(a[1..].to_vec()).cmp(&Data::List(b[1..].to_vec())),
                    x => x,
                },
            },
            (Data::List(_), Data::Int(b)) => self.cmp(&Data::List(vec![Data::Int(*b)])),
            (Data::Int(a), Data::List(_)) => Data::List(vec![Data::Int(*a)]).cmp(other),
        }
    }
}

impl PartialOrd for Data {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn main() {
    let input = fs::read_to_string("input/day-13.txt").unwrap();

    let mut input_pairs = Vec::new();
    for chunk in input.split("\n\n") {
        let lines: Vec<_> = chunk.lines().collect();
        input_pairs.push((Data::parse(lines[0]).0, Data::parse(lines[1]).0));
    }

    let mut sum = 0;
    for (i, (a, b)) in input_pairs.iter().enumerate() {
        if a < b {
            sum += i + 1;
        }
    }
    println!("Part 1: {sum}");

    let div1 = Data::List(vec![Data::List(vec![Data::Int(2)])]);
    let div2 = Data::List(vec![Data::List(vec![Data::Int(6)])]);
    let mut all_packets = Vec::new();
    for line in input.lines() {
        if !line.is_empty() {
            all_packets.push(Data::parse(line).0);
        }
    }
    all_packets.push(div1.clone());
    all_packets.push(div2.clone());
    all_packets.sort();
    let idx1 = all_packets.iter().position(|x| *x == div1).unwrap();
    let idx2 = all_packets.iter().position(|x| *x == div2).unwrap();
    println!("Part 2: {}", (idx1 + 1) * (idx2 + 1));
}
