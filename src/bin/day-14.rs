use std::{fs, collections::HashSet};
use itertools::Itertools;

fn find_landing_point(rocks: &HashSet<(i32, i32)>, sand: &HashSet<(i32, i32)>, floor_y: i32) -> (i32, i32) {
    let (mut x, mut y) = (500, 0);
    loop {
        if let Some(empty) = [(x, y + 1), (x - 1, y + 1), (x + 1, y + 1)].iter().find(|c| !rocks.contains(c) && !sand.contains(c) && c.1 < floor_y) {
            (x, y) = *empty;
        } else {
            return (x, y); // blocked
        }
    }
}

fn main() {
    let input = fs::read_to_string("input/day-14.txt").unwrap();
    let mut rocks = HashSet::new();
    for line in input.lines() {
        for (a, b) in line.split(" -> ").tuple_windows() {
            let (ax, ay) = a.split(',').map(|x| x.parse::<i32>().unwrap()).collect_tuple().unwrap();
            let (bx, by) = b.split(',').map(|x| x.parse::<i32>().unwrap()).collect_tuple().unwrap();
            for x in ax.min(bx) ..= ax.max(bx) {
                for y in ay.min(by) ..= ay.max(by) {
                    rocks.insert((x, y));
                }
            }
        }
    }

    let bottom = rocks.iter().map(|(_, y)| *y).max().unwrap() + 2;
    let mut sand = HashSet::new();
    loop {
        let (x, y) = find_landing_point(&rocks, &sand, bottom);
        if y == bottom - 1 {
            break;
        }
        sand.insert((x, y));
    }
    println!("Part 1: {}", sand.len());

    loop {
        let (x, y) = find_landing_point(&rocks, &sand, bottom);
        sand.insert((x, y));
        if (x, y) == (500, 0) {
            break;
        }
    }
    println!("Part 2: {}", sand.len());
}
