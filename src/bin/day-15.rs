/*
A hard one!
The only way I can think of to do part 2 is to track the polygonal region(s) that might contain the
hidden beacon, paring away the areas where we know it can't be as we see them.
Polygon subtraction is annoying so I'm using a helper package to do the heavy lifting.
*/

use std::{fs, collections::{HashSet, HashMap}};
use regex::Regex;
use geo::{Polygon, LineString, BooleanOps, MultiPolygon, Centroid};

fn manhattan((x1, y1): (i32, i32), (x2, y2): (i32, i32)) -> i32 {
    (x1 - x2).abs() + (y1 - y2).abs()
}

fn main() {
    let input = fs::read_to_string("input/day-15.txt").unwrap();
    let mut sensors = HashMap::new();
    let re = Regex::new(r"Sensor at x=(.*), y=(.*): closest beacon is at x=(.*), y=(.*)").unwrap();
    for cap in re.captures_iter(&input) {
        let sensor: (i32, i32) = (cap[1].parse().unwrap(), cap[2].parse().unwrap());
        let beacon: (i32, i32) = (cap[3].parse().unwrap(), cap[4].parse().unwrap());
        sensors.insert(sensor, beacon);
    }

    let p1_target_y = 2000000;
    let mut p1_x_taken = HashSet::new();
    for (&(sx, sy), &b) in sensors.iter() {
        let beacon_dist = manhattan((sx, sy), b);
        let y_dist = (sy - p1_target_y).abs();
        for dx in 0 ..= (beacon_dist - y_dist) {
            p1_x_taken.insert(sx - dx);
            p1_x_taken.insert(sx + dx);
        }
    }
    for (x, y) in sensors.keys() {
        if *y == p1_target_y {
            p1_x_taken.remove(x);
        }
    }
    println!("Part 1: {}", p1_x_taken.len());

    let mut safe_poly = MultiPolygon::new(vec![
        Polygon::new(
            LineString::from(vec![(0., 0.), (0., 4000000.), (4000000., 4000000.), (4000000., 0.)]),
            vec![])]);
    for (&(sx, sy), &b) in sensors.iter() {
        let d = manhattan((sx, sy), b);
        let diamond = Polygon::new(
            LineString::from(vec![
                ((sx - d) as f32, sy as f32),
                (sx as f32, (sy + d) as f32),
                ((sx + d) as f32, sy as f32),
                (sx as f32, (sy - d) as f32)]),
            vec![]);
        safe_poly = safe_poly.difference(&MultiPolygon::new(vec![diamond]));
    }

    assert!(safe_poly.iter().count() == 1);
    let centroid = safe_poly.iter().next().unwrap().centroid().unwrap();
    println!("Part 2: {}", (centroid.x() as u64 * 4000000) + centroid.y() as u64);
}
