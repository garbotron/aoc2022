/* This was awful! and I clearly didn't have the right approach. But I got it working and I don't want to revisit it! */

use std::{fs, collections::HashMap};
use regex::Regex;

type RoomMask = u64;

struct Room {
    flow_rate: i32,
    leads_to: HashMap<usize, i32>,
}

struct Map {
    rooms: Vec<Room>,
    room_idxs_sorted_by_flow_rate: Vec<usize>,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct StateP1 {
    room: usize,
    valves_open: RoomMask,
    time_remaining: i32,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct StateP2 {
    my_room: usize,
    elephant_room: usize,
    valves_open: RoomMask,
    time_remaining: i32,
    my_travel_time: i32,
    elephant_travel_time: i32,
}

struct ExplorationCache<State> {
    best_final: i32,
    best_per_state: HashMap<State, i32>,
}

enum Action {
    Wait,
    OpenValve,
    MoveTo((usize, i32)),
}

fn get_actions(map: &Map, room: usize, valves_open: RoomMask) -> Vec<Action> {
    let mut actions = Vec::new();
    if map.rooms[room].flow_rate != 0 && (valves_open & (1 << room)) == 0 {
        actions.push(Action::OpenValve);
    }
    for (&x, &cost) in map.rooms[room].leads_to.iter() {
        actions.push(Action::MoveTo((x, cost)));
    }
    actions
}

fn explore_p1(map: &Map, cache: &mut ExplorationCache<StateP1>, state: StateP1, score: i32) {
    if state.time_remaining == 0 {
        cache.best_final = cache.best_final.max(score);
        return;
    }

    if let Some(prev) = cache.best_per_state.get(&state) {
        if *prev >= score {
            return;
        }
    }

    for i in 1 ..= state.time_remaining {
        cache.best_per_state.insert(StateP1 { time_remaining: i, ..state }, score);
    }

    for action in get_actions(map, state.room, state.valves_open).iter() {
        match action {
            Action::Wait => (),
            Action::OpenValve => {
                let mut new_state = state.clone();
                new_state.time_remaining -= 1;
                new_state.valves_open |= 1 << state.room;
                let new_score = score + new_state.time_remaining * map.rooms[state.room].flow_rate;
                explore_p1(map, cache, new_state, new_score);
            },
            Action::MoveTo((i, cost)) => {
                if *cost < state.time_remaining {
                    let mut new_state = state.clone();
                    new_state.time_remaining -= cost;
                    new_state.room = *i;
                    explore_p1(map, cache, new_state, score);
                }
            },
        }
    }
}

fn explore_p2(map: &Map, cache: &mut ExplorationCache<StateP2>, state: StateP2, score: i32) {
    if state.time_remaining == 0 {
        cache.best_final = cache.best_final.max(score);
        return;
    }

    if let Some(prev) = cache.best_per_state.get(&state) {
        if *prev >= score {
            return;
        }
    }

    // Naive upper bound calculation. With me and the elephant both opening valves super efficiently,
    // we can open one per minute. Guess at a best-case scenario where we open the next biggest valve
    // once per minute, for the rest of the remaining time. If this isn't good enough, give up.
    let mut upper_bound = score;
    let mut upper_bound_time_remaining = state.time_remaining;
    for &i in map.room_idxs_sorted_by_flow_rate.iter() {
        if map.rooms[i].flow_rate == 0 { break }
        if state.valves_open & (1 << i) == 0 {
            upper_bound += map.rooms[i].flow_rate * upper_bound_time_remaining;
            upper_bound_time_remaining -= 1;
            if upper_bound_time_remaining == 0 {
                break;
            }
        }
    }
    if upper_bound <= cache.best_final {
        return;
    }

    for i in 1 ..= state.time_remaining {
        cache.best_per_state.insert(StateP2 { time_remaining: i, ..state }, score);
        cache.best_per_state.insert(StateP2 {
            time_remaining: i,
            my_room: state.elephant_room,
            elephant_room: state.my_room,
            my_travel_time: state.elephant_travel_time,
            elephant_travel_time: state.my_travel_time,
            ..state },
        score);
    }

    let my_actions = if state.my_travel_time > 0 {
        vec![Action::Wait]
    } else {
        get_actions(map, state.my_room, state.valves_open)
    };

    let elephant_actions = if state.elephant_travel_time > 0 {
        vec![Action::Wait]
    } else {
        get_actions(map, state.elephant_room, state.valves_open)
    };

    for my_action in my_actions.iter() {
        for elephant_action in elephant_actions.iter() {
            let mut new_state = state.clone();
            let mut new_score = score;
            new_state.time_remaining -= 1;

            match my_action {
                Action::Wait => new_state.my_travel_time -= 1,
                Action::MoveTo((i, cost)) => {
                    new_state.my_room = *i;
                    new_state.my_travel_time = *cost - 1;
                },
                Action::OpenValve => {
                    new_state.valves_open |= 1 << state.my_room;
                    new_score += new_state.time_remaining * map.rooms[state.my_room].flow_rate;
                },
            }

            match elephant_action {
                Action::Wait => new_state.elephant_travel_time -= 1,
                Action::MoveTo((i, cost)) => {
                    new_state.elephant_room = *i;
                    new_state.elephant_travel_time = *cost - 1;
                },
                Action::OpenValve => {
                    if new_state.valves_open & (1 << state.elephant_room) != 0 {
                        continue; // already opened by me
                    }
                    new_state.valves_open |= 1 << state.elephant_room;
                    new_score += new_state.time_remaining * map.rooms[state.elephant_room].flow_rate;
                },
            }

            explore_p2(map, cache, new_state, new_score);
        }
    }
}

fn collapse_room(map: &mut Map, room: usize) {
    let connections = map.rooms[room].leads_to.clone();
    for (&room_a, &cost_a) in connections.iter() {
        for (&room_b, &cost_b) in connections.iter() {
            if room_a != room_b {
                let new_cost = cost_a + cost_b;
                let old_cost = *map.rooms[room_a].leads_to.get(&room_b).unwrap_or(&i32::MAX);
                if new_cost < old_cost {
                    map.rooms[room_a].leads_to.insert(room_b, new_cost);
                }
            }
        }
    }
    for (&other, _) in connections.iter() {
        map.rooms[other].leads_to.retain(|&x, _| x != room);
    }
}

// Heuristic lower bound. Keep jumping to the next highest value node blindly.
fn lower_bound_p2(map: &Map, starting_room: usize) -> i32 {
    let mut score = 0;
    let mut valves_open = 0;
    let mut my_room = starting_room;
    let mut elephant_room = starting_room;
    let mut my_remaining_time = 26;
    let mut elephant_remaining_time = 26;

    loop {
        let my_dests: Vec<_> = map.rooms[my_room].leads_to.iter().filter(|(i, cost)| (valves_open & (1 << **i)) == 0 && **cost + 1 < my_remaining_time).collect();
        let elephant_dests: Vec<_> = map.rooms[elephant_room].leads_to.iter().filter(|(i, cost)| (valves_open & (1 << **i)) == 0 && **cost + 1 < elephant_remaining_time).collect();
        if my_dests.is_empty() && elephant_dests.is_empty() {
            break;
        } else if my_dests.len() > elephant_dests.len() {
            let (dest, cost) = *my_dests.iter().max_by_key(|(i, cost)| (my_remaining_time - (**cost + 1)) * map.rooms[**i].flow_rate).unwrap();
            score += (my_remaining_time - (*cost + 1)) * map.rooms[*dest].flow_rate;
            my_remaining_time -= *cost + 1;
            my_room = *dest;
            valves_open |= 1 << *dest;
        } else {
            let (dest, cost) = *elephant_dests.iter().max_by_key(|(i, cost)| (elephant_remaining_time - (**cost + 1)) * map.rooms[**i].flow_rate).unwrap();
            score += (elephant_remaining_time - (*cost + 1)) * map.rooms[*dest].flow_rate;
            elephant_remaining_time -= *cost + 1;
            elephant_room = *dest;
            valves_open |= 1 << *dest;
        }
    }

    score
}

fn main() {
    let input = fs::read_to_string("input/day-16.txt").unwrap();

    let re = Regex::new(r"Valve (.*) has flow rate=(.*); tunnels? leads? to valves? (.*)").unwrap();

    let mut room_indices = HashMap::new();
    for cap in re.captures_iter(&input) {
        room_indices.insert(cap[1].to_string(), room_indices.len());
    }

    let mut map = Map { rooms: Vec::new(), room_idxs_sorted_by_flow_rate: Vec::new() };
    for cap in re.captures_iter(&input) {
        let mut leads_to = HashMap::new();
        for s in cap[3].split(", ") {
            let idx = *room_indices.get(s).unwrap();
            leads_to.insert(idx, 1);
        }
        map.room_idxs_sorted_by_flow_rate.push(map.rooms.len());
        map.rooms.push(Room { flow_rate: cap[2].parse().unwrap(), leads_to });
    }

    for i in 0..map.rooms.len() {
        if map.rooms[i].flow_rate == 0 {
            collapse_room(&mut map, i);
        }
    }

    map.room_idxs_sorted_by_flow_rate.sort_by_key(|x| -map.rooms[*x].flow_rate);
    let starting_room = *room_indices.get(&"AA".to_string()).unwrap();

    let init_state_p1 = StateP1 { room: starting_room, valves_open: 0, time_remaining: 30 };
    let mut cache_p1 = ExplorationCache { best_final: 0, best_per_state: HashMap::new() };
    explore_p1(&map, &mut cache_p1, init_state_p1, 0);
    println!("Part 1: {}", cache_p1.best_final);

    let init_state_p2 = StateP2 { my_room: starting_room, elephant_room: starting_room, valves_open: 0, time_remaining: 26, my_travel_time: 0, elephant_travel_time: 0 };
    let mut cache_p2 = ExplorationCache { best_final: lower_bound_p2(&map, starting_room), best_per_state: HashMap::new() };
    explore_p2(&map, &mut cache_p2, init_state_p2, 0);
    println!("Part 2: {}", cache_p2.best_final);
}
