use std::fs;

/* Model the rock tower as a vector of 7-bit numbers, starting at the bottom growing upward.
 * This makes checking for collision fast (bitwise and) and shifting left and right simple (bitshift). */
const ROCK_SHAPES: &[&[u8]] = &[
    &[0x1e],
    &[0x8, 0x1c, 0x8],
    &[0x1c, 0x4, 0x4],
    &[0x10, 0x10, 0x10, 0x10],
    &[0x18, 0x18],
];

struct JetStream {
    jets: Vec<char>,
    idx: usize,
}

struct RockStream {
    idx: usize,
}

fn shift_7b(line: u8, shift: i32) -> u8 {
    if shift < 0 {
        (line << -shift) & 0x7f
    } else {
        line >> shift
    }
}

fn collides(tower: &Vec<u8>, rock: &[u8], shift: i32, dy: i32) -> bool {
    if tower.len() as i32 + dy < 0 {
        return true;
    }
    if rock.iter().any(|&x| x != shift_7b(shift_7b(x, shift), -shift)) {
        return true;
    }
    for (i, &rock_row) in rock.iter().enumerate() {
        let tower_row = ((tower.len() + i) as i32 + dy) as usize;
        if tower_row < tower.len() && (tower[tower_row] & shift_7b(rock_row, shift)) != 0 {
            return true;
        }
    }
    false
}

fn simulate_rock(tower: &mut Vec<u8>, jets: &mut JetStream, rocks: &mut RockStream) {
    let rock = ROCK_SHAPES[rocks.idx];
    rocks.idx = (rocks.idx + 1) % ROCK_SHAPES.len();
    let mut shift = 0;
    let mut dy = 3;
    loop {
        let jet = jets.jets[jets.idx];
        jets.idx = (jets.idx + 1) % jets.jets.len();

        if jet == '<' {
            if !collides(&tower, rock, shift - 1, dy) {
                shift -= 1;
            }
        } else {
            if !collides(&tower, rock, shift + 1, dy) {
                shift += 1;
            }
        }

        if collides(&tower, rock, shift, dy - 1) {
            let prev_h = tower.len();
            for _ in 0..(rock.len() as i32 + dy) {
                tower.push(0);
            }
            for (i, &x) in rock.iter().enumerate() {
                tower[((prev_h + i) as i32 + dy) as usize] |= shift_7b(x, shift);
            }
            break;
        } else {
            dy -= 1;
        }
    }
}

fn main() {
    let input = fs::read_to_string("input/day-17.txt").unwrap();
    let jets: Vec<_> = input.trim().chars().collect();

    let mut p1_tower = Vec::new();
    let mut p1_jets = JetStream { jets: jets.clone(), idx: 0 };
    let mut p1_rocks = RockStream { idx: 0 };
    for _ in 0..2022 {
        simulate_rock(&mut p1_tower, &mut p1_jets, &mut p1_rocks);
    }
    println!("Part 1: {}", p1_tower.len());

    /* For part 2, we need to find the cycle since we can't simulate all of it.
     * Since we have cycling rocks as well as jets, we need find the moment where they coincide.
     * First, we set up the tower with a large number of rocks to get an initial shape in place. */
    let initial_rock_count = 100000;
    let mut p2_tower = Vec::new();
    let mut p2_jets = JetStream { jets: jets.clone(), idx: 0 };
    let mut p2_rocks = RockStream { idx: 0 };
    for _ in 0..initial_rock_count {
        simulate_rock(&mut p2_tower, &mut p2_jets, &mut p2_rocks);
    }

    /* Now, cache the current jet/rock idx and then simulate more rocks until we hit a duplicate. */
    let initial_tower_height = p2_tower.len();
    let initial_jet = p2_jets.idx;
    let initial_rock = p2_rocks.idx;
    let mut cycle_len = 0u64;
    let mut remainder_heights = Vec::new();
    loop {
        remainder_heights.push(p2_tower.len() - initial_tower_height);
        simulate_rock(&mut p2_tower, &mut p2_jets, &mut p2_rocks);
        cycle_len += 1;
        if p2_jets.idx == initial_jet && p2_rocks.idx == initial_rock {
            break;
        }
    }

    /* With this information we can calculate what will happen after a gazillion rocks. */
    let height_per_cycle = (p2_tower.len() - initial_tower_height) as u64;
    let cycles_after_init = (1000000000000 - initial_rock_count) / cycle_len;
    let remainder = ((1000000000000 - initial_rock_count) % cycle_len) as usize;
    let total_height = initial_tower_height as u64 + (cycles_after_init * height_per_cycle) + remainder_heights[remainder] as u64;
    println!("Part 2: {total_height}");
}
