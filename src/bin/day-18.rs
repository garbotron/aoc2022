use std::{fs, collections::{HashSet, HashMap}};

fn cube_faces((x, y, z): (i32, i32, i32)) -> Vec< Vec<(i32, i32, i32)>> {
    let mut faces = vec![
        vec![(x, y, z), (x + 1, y, z), (x + 1, y + 1, z), (x, y + 1, z)],
        vec![(x, y, z), (x + 1, y, z), (x + 1, y, z + 1), (x, y, z + 1)],
        vec![(x, y, z), (x, y + 1, z), (x, y + 1, z + 1), (x, y, z + 1)],
        vec![(x, y, z + 1), (x + 1, y, z + 1), (x + 1, y + 1, z + 1), (x, y + 1, z + 1)],
        vec![(x, y + 1, z), (x + 1, y + 1, z), (x + 1, y + 1, z + 1), (x, y + 1, z + 1)],
        vec![(x + 1, y, z), (x + 1, y + 1, z), (x + 1, y + 1, z + 1), (x + 1, y, z + 1)],
    ];
    for x in faces.iter_mut() {
        x.sort();
    }
    faces
}

fn main() {
    let input = fs::read_to_string("input/day-18.txt").unwrap();
    let mut vertices = Vec::new();
    for line in input.lines() {
        let split: Vec<_> = line.split(',').map(|x| x.parse::<i32>().unwrap()).collect();
        vertices.push((split[0], split[1], split[2]));
    }

    let mut faces = HashMap::new();
    for vertex in vertices.iter() {
        for face in cube_faces(*vertex) {
            if let Some(x) = faces.get_mut(&face) {
                *x += 1;
            } else {
                faces.insert(face, 1);
            }
        }
    }
    println!("Part 1: {}", faces.values().filter(|x| **x == 1).count());

    let min_x = vertices.iter().map(|&(x, _, _)| x).min().unwrap() - 1;
    let max_x = vertices.iter().map(|&(x, _, _)| x).max().unwrap() + 1;
    let min_y = vertices.iter().map(|&(_, y, _)| y).min().unwrap() - 1;
    let max_y = vertices.iter().map(|&(_, y, _)| y).max().unwrap() + 1;
    let min_z = vertices.iter().map(|&(_, _, z)| z).min().unwrap() - 1;
    let max_z = vertices.iter().map(|&(_, _, z)| z).max().unwrap() + 1;

    let oob = |(x, y, z)| x < min_x || x > max_x || y < min_y || y > max_y || z < min_z || z > max_z;

    let neighbors = |(x, y, z)| [
        (x - 1, y, z), (x + 1, y, z),
        (x, y - 1, z), (x, y + 1, z),
        (x, y, z - 1), (x, y, z + 1),
    ];

    let lava: HashSet<_> = vertices.iter().copied().collect();
    let mut collisions = 0;
    let mut explored = HashSet::new();
    let mut to_explore = vec![(min_x, min_y, min_z)];
    while let Some(cur) = to_explore.pop() {
        if !explored.contains(&cur) {
            explored.insert(cur);
            for n in neighbors(cur) {
                if !oob(n) {
                    if lava.contains(&n) {
                        collisions += 1;
                    } else {
                        to_explore.push(n);
                    }
                }
            }
        }
    }
    println!("Part 2: {collisions}");
}
