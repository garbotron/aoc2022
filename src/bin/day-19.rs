use std::{fs, collections::HashMap};
use regex::Regex;

struct Blueprint {
    orebot_cost_ore: i32,
    claybot_cost_ore: i32,
    obsidibot_cost_ore: i32,
    obsidibot_cost_clay: i32,
    geodebot_cost_ore: i32,
    geodebot_cost_obsidian: i32,
}

struct State {
    time_remaining: i32,
    ore: i32,
    clay: i32,
    obsidian: i32,
    orebots: i32,
    claybots: i32,
    obsidibots: i32,
}

struct Metadata {
    cache: HashMap<u64, i32>,
    best_score: i32,
    max_orebots: i32,
    max_claybots: i32,
    max_obsidibots: i32,
}

fn encode_state(state: &State, meta: &Metadata) -> u64 {
    // Encode each field into 8 bits.
    // The max number of bots of any given type is a pretty low number (based on construction cost) so all the bot counts will fit.
    // The max number of resources is moot once you've got the max bot count. Otherwise we just cap it at 255 - it'll never get there without hitting the max bot count.
    let mut ret = 0u64;
    let mut shift_in = |v| ret = (ret << 8) | (v as u64);
    shift_in(state.time_remaining);
    shift_in(state.orebots);
    shift_in(state.claybots);
    shift_in(state.obsidibots);
    shift_in(if state.orebots < meta.max_orebots { state.ore.min(255) } else { 255 });
    shift_in(if state.claybots < meta.max_claybots { state.clay.min(255) } else { 255 });
    shift_in(if state.obsidibots < meta.max_obsidibots { state.obsidian.min(255) } else { 255 });
    ret
}

fn explore(blueprint: &Blueprint, meta: &mut Metadata, score: i32, state: State) {
    if state.time_remaining == 0 {
        meta.best_score = meta.best_score.max(score);
        return;
    }

    let encoded = encode_state(&state, meta);
    if let Some(&prev) = meta.cache.get(&encoded) {
        if score <= prev {
            return;
        }
    }

    meta.cache.insert(encoded, score);

    let can_build_orebot = state.ore >= blueprint.orebot_cost_ore;
    let can_build_claybot = state.ore >= blueprint.claybot_cost_ore;
    let can_build_obsidibot = state.ore >= blueprint.obsidibot_cost_ore && state.clay >= blueprint.obsidibot_cost_clay;
    let can_build_geodebot = state.ore >= blueprint.geodebot_cost_ore && state.obsidian >= blueprint.geodebot_cost_obsidian;

    let state = State {
        time_remaining: state.time_remaining - 1,
        ore: state.ore + state.orebots,
        clay: state.clay + state.claybots,
        obsidian: state.obsidian + state.obsidibots,
        ..state };

    if can_build_orebot && state.orebots < meta.max_orebots {
        explore(blueprint, meta, score, State {
            orebots: state.orebots + 1,
            ore: state.ore - blueprint.orebot_cost_ore,
            ..state
        });
    }

    if can_build_claybot && state.orebots < meta.max_claybots {
        explore(blueprint, meta, score, State {
            claybots: state.claybots + 1,
            ore: state.ore - blueprint.claybot_cost_ore,
            ..state
        });
    }

    if can_build_obsidibot && state.orebots < meta.max_obsidibots {
        explore(blueprint, meta, score, State {
            obsidibots: state.obsidibots + 1,
            ore: state.ore - blueprint.obsidibot_cost_ore,
            clay: state.clay - blueprint.obsidibot_cost_clay,
            ..state
        });
    }

    if can_build_geodebot {
        explore(blueprint, meta, score + state.time_remaining, State {
            ore: state.ore - blueprint.geodebot_cost_ore,
            obsidian: state.obsidian - blueprint.geodebot_cost_obsidian,
            ..state
        });
    }

    if !(can_build_orebot && can_build_claybot && can_build_obsidibot && can_build_geodebot) {
        explore(blueprint, meta, score, state); // we can always do nothing (unless we can build everything, in which case it's never the right choice)
    }
}

fn best_geode_production(blueprint: &Blueprint, time: i32) -> i32 {
    let initial_state = State { time_remaining: time, ore: 0, clay: 0, obsidian: 0, orebots: 1, claybots: 0, obsidibots: 0 };
    let mut meta = Metadata {
        cache: HashMap::new(),
        best_score: 0,
        max_orebots: *[blueprint.orebot_cost_ore, blueprint.claybot_cost_ore, blueprint.obsidibot_cost_ore, blueprint.geodebot_cost_ore].iter().max().unwrap(),
        max_claybots: blueprint.obsidibot_cost_clay,
        max_obsidibots: blueprint.geodebot_cost_obsidian,
    };
    explore(blueprint, &mut meta, 0, initial_state);
    meta.best_score
}

fn main() {
    let input = fs::read_to_string("input/day-19.txt").unwrap();
    let re = Regex::new(r"Blueprint [0-9]+: Each ore robot costs ([0-9]+) ore. Each clay robot costs ([0-9]+) ore. Each obsidian robot costs ([0-9]+) ore and ([0-9]+) clay. Each geode robot costs ([0-9]+) ore and ([0-9]+) obsidian.").unwrap();
    let mut blueprints = Vec::new();
    for cap in re.captures_iter(&input) {
        blueprints.push(Blueprint {
            orebot_cost_ore: cap[1].parse().unwrap(),
            claybot_cost_ore: cap[2].parse().unwrap(),
            obsidibot_cost_ore: cap[3].parse().unwrap(),
            obsidibot_cost_clay: cap[4].parse().unwrap(),
            geodebot_cost_ore: cap[5].parse().unwrap(),
            geodebot_cost_obsidian: cap[6].parse().unwrap(),
        })
    }

    let mut p1_total = 0;
    for (i, b) in blueprints.iter().enumerate() {
        p1_total += (i as i32 + 1) * best_geode_production(b, 24);
    }
    println!("Part 1: {p1_total}");

    let mut p2_total = 1;
    for b in blueprints.iter().take(3) {
        p2_total *= best_geode_production(b, 32);
    }
    println!("Part 2: {p2_total}");
}
