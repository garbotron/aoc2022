use std::fs;

// Linked lists in Rust suck, so just map the forward/back links in parallel vectors.
fn mix(numbers: &Vec<i64>, fwd_links: &mut Vec<usize>, back_links: &mut Vec<usize>) {
    for (i, &num) in numbers.iter().enumerate() {
        if num == 0 {
            continue; // nothing to do
        }

        // Remove it from the list first.
        back_links[fwd_links[i]] = back_links[i];
        fwd_links[back_links[i]] = fwd_links[i];

        // Now follow the pointers N times (mod list length) (-1 for the removed entry), then insert.
        if num > 0 {
            let mut cur = fwd_links[i];
            for _ in 1..(num % (numbers.len() - 1) as i64) {
                cur = fwd_links[cur];
            }
            back_links[i] = cur;
            fwd_links[i] = fwd_links[cur];
            fwd_links[cur] = i;
            back_links[fwd_links[i]] = i;
        } else {
            let mut cur = back_links[i];
            for _ in 1..(-num % (numbers.len() - 1) as i64) {
                cur = back_links[cur];
            }
            fwd_links[i] = cur;
            back_links[i] = back_links[cur];
            back_links[cur] = i;
            fwd_links[back_links[i]] = i;
        }
    }
}

fn grove_coords(numbers: &Vec<i64>, fwd_links: &Vec<usize>) -> i64 {
    let mut cursor = numbers.iter().position(|&x| x == 0).unwrap();
    let mut sum = 0;
    for _ in 0..3 {
        for _ in 0..1000 {
            cursor = fwd_links[cursor];
        }
        sum += numbers[cursor];
    }
    sum
}

fn main() {
    let input = fs::read_to_string("input/day-20.txt").unwrap();
    let numbers: Vec<_> = input.lines().map(|x| x.parse::<i64>().unwrap()).collect();
    let init_fwd_links: Vec<_> = (0..numbers.len()).map(|x| (x + 1) % numbers.len()).collect();
    let init_back_links: Vec<_> = (0..numbers.len()).map(|x| if x == 0 { numbers.len() - 1 } else { x - 1 }).collect();

    let mut p1_fwd_links = init_fwd_links.clone();
    let mut p1_back_links = init_back_links.clone();
    mix(&numbers, &mut p1_fwd_links, &mut p1_back_links);
    println!("Part 1: {}", grove_coords(&numbers, &p1_fwd_links));

    let numbers_w_key: Vec<_> = numbers.iter().map(|&x| x * 811589153).collect();
    let mut p2_fwd_links = init_fwd_links.clone();
    let mut p2_back_links = init_back_links.clone();
    for _ in 0..10 {
        mix(&numbers_w_key, &mut p2_fwd_links, &mut p2_back_links);
    }
    println!("Part 2: {}", grove_coords(&numbers_w_key, &p2_fwd_links));
}
