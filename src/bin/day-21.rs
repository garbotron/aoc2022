/*
Expression trees! Build up a tree like this:

      =
     / \
   40   *
       / \
      4   -
         / \
        +   3
       / \
      x   2

Then start collapsing! Any arithmetic operation on two numbers can be collapsed.
For part 2, we need to also collapse any expression of the form "humn (operator) (expression) = (expression)".
We can do this by applying the inverse operator to both sides.

div both sides by 4

      =
     / \
   10   -
       / \
      +   3
     / \
    x   2

add 3 to both sides

      =
     / \
   13   +
       / \
      x   2

sub 2 from both sides

...
*/

use std::{fs, collections::HashMap};
use itertools::Itertools;

enum Op { Add, Sub, Mul, Div, Eql }

enum Node {
    Val(i64),
    Oper(Oper),
    Human,
}

struct Oper {
    op: Op,
    lhs: Box<Node>,
    rhs: Box<Node>,
}

fn build_tree_from_input(job_map: &HashMap<&str, &str>, name: &str) -> Node {
    let job_split = job_map.get(name).unwrap().split(' ').collect_vec();
    if job_split.len() == 1 {
        if job_split[0] == "x" {
            Node::Human
        } else {
            Node::Val(job_split[0].parse().unwrap())
        }
    } else {
        Node::Oper(Oper {
            op: match job_split[1] {
                "+" => Op::Add,
                "-" => Op::Sub,
                "*" => Op::Mul,
                "/" => Op::Div,
                "=" => Op::Eql,
                _ => panic!(),
            },
            lhs: Box::new(build_tree_from_input(job_map, job_split[0])),
            rhs: Box::new(build_tree_from_input(job_map, job_split[2])),
        })
    }
}

fn new_oper(op: Op, lhs: Node, rhs: Node) -> Node {
    Node::Oper(Oper { op, lhs: Box::new(lhs), rhs: Box::new(rhs) })
}

fn inverse_op(op: Op) -> Op {
    match op {
        Op::Add => Op::Sub,
        Op::Sub => Op::Add,
        Op::Mul => Op::Div,
        Op::Div => Op::Mul,
        _ => panic!(),
    }
}

fn collapse(node: Node) -> Node {
    match node {
        Node::Oper(Oper { op: Op::Eql, lhs, rhs }) => {
            let lhs = collapse(*lhs);
            let rhs = collapse(*rhs);
            if let Node::Human = lhs {
                rhs
            } else if let Node::Human = rhs {
                lhs
            } else if let Node::Oper(op) = lhs {
                collapse(new_oper(Op::Eql, *op.lhs, new_oper(inverse_op(op.op), rhs, *op.rhs)))
            } else if let Node::Oper(op) = rhs {
                collapse(new_oper(Op::Eql, new_oper(inverse_op(op.op), lhs, *op.rhs), *op.lhs))
            } else {
                panic!()
            }
        },
        Node::Oper(oper) => {
            let lhs = collapse(*oper.lhs);
            let rhs = collapse(*oper.rhs);
            match (oper.op, lhs, rhs) {
                (Op::Add, Node::Val(x), Node::Val(y)) => Node::Val(x + y),
                (Op::Sub, Node::Val(x), Node::Val(y)) => Node::Val(x - y),
                (Op::Mul, Node::Val(x), Node::Val(y)) => Node::Val(x * y),
                (Op::Div, Node::Val(x), Node::Val(y)) => Node::Val(x / y),
                (Op::Add, Node::Val(x), y) => new_oper(Op::Add, y, Node::Val(x)),
                (Op::Mul, Node::Val(x), y) => new_oper(Op::Mul, y, Node::Val(x)),
                (op, x, y) => new_oper(op, x, y),
            }
        },
        x => x,
    }
}

fn main() {
    let input = fs::read_to_string("input/day-21.txt").unwrap();

    let mut job_map = HashMap::new();
    for line in input.lines() {
        let (name, job) = line.split(": ").collect_tuple().unwrap();
        job_map.insert(name, job);
    }

    match collapse(build_tree_from_input(&job_map, "root")) {
        Node::Val(x) => println!("Part 1: {x}"),
        _ => panic!(),
    }

    let new_root_job = job_map.get("root").unwrap().replace("+", "=");
    job_map.insert("root", &new_root_job);
    job_map.insert("humn", &"x");

    match collapse(build_tree_from_input(&job_map, "root")) {
        Node::Val(x) => println!("Part 2: {x}"),
        _ => panic!(),
    }
}
