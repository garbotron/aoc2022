use std::{fs, collections::{HashSet, HashMap}};
use itertools::Itertools;

type Pos = (i32, i32);
type Dir = (i32, i32);

const LEFT: Dir = (-1, 0);
const RIGHT: Dir = (1, 0);
const UP: Dir = (0, -1);
const DOWN: Dir = (0, 1);
const DIRS: &[Dir] = &[LEFT, RIGHT, UP, DOWN];

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct State { pos: Pos, dir: Dir }

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
enum Instr { Forward(i32), CW, CCW }

fn inv(p: Dir) -> Dir {
    (-p.0, -p.1)
}

fn go(map: &HashMap<State, State>, state: State, instr: Instr) -> State {
    match instr {
        Instr::Forward(dist) => {
            let mut state = state;
            for _ in 0..dist {
                if let Some(&next) = map.get(&state) {
                    state = next;
                }
            }
            state
        },
        Instr::CW => State { dir: (-state.dir.1, state.dir.0), ..state },
        Instr::CCW => State { dir: (state.dir.1, -state.dir.0), ..state },
    }
}

fn score(state: &State) -> i32 {
    1000 * (state.pos.1 + 1) + 4 * (state.pos.0 + 1) + match state.dir {
        RIGHT => 0,
        DOWN => 1,
        LEFT => 2,
        UP => 3,
        _ => panic!(),
    }
}

fn edge(len: i32, (pos, dir): (Pos, Dir)) -> Vec<Pos> {
    (0..len)
        .map(|i| (pos.0 + (dir.0 * i), pos.1 + (dir.1 * i)))
        .collect()
}

fn main() {
    let input = fs::read_to_string("input/day-22.txt").unwrap();
    let (input_map, input_instrs) = input.split("\n\n").collect_tuple().unwrap();

    let mut open = HashSet::new();
    let mut walls = HashSet::new();
    for (y, line) in input_map.lines().enumerate() {
        for (x, ch) in line.chars().enumerate() {
            match ch {
                '.' => { open.insert((x as i32, y as i32)); },
                '#' => { walls.insert((x as i32, y as i32)); }
                _ => (),
            }
        }
    }

    let instrs = input_instrs
        .trim()
        .chars()
        .group_by(|x| x.is_digit(10))
        .into_iter()
        .map(|(_, grp)| match grp.collect::<String>().as_str() {
            "R" => Instr::CW,
            "L" => Instr::CCW,
            s => Instr::Forward(s.parse().unwrap()) })
        .collect_vec();

    let init_y = 0;
    let init_x = (0..i32::MAX).find(|&x| open.contains(&(x, init_y))).unwrap();
    let init_state = State { pos: (init_x, init_y), dir: RIGHT };

    // Set up the map for all normally navigable paths (no wraparound)
    let mut common_map = HashMap::new();
    for &pos in open.iter() {
        for &dir in DIRS {
            let next = (pos.0 + dir.0, pos.1 + dir.1);
            if open.contains(&next) {
                common_map.insert(State { pos, dir }, State { pos: next, dir });
            }
        }
    }

    // Set up the map for P1 (Pac-Man style wraparound)
    let mut p1_map = common_map.clone();
    for &pos in open.iter() {
        for &dir in DIRS {
            let state = State { pos, dir };
            if !p1_map.contains_key(&state) && !walls.contains(&(pos.0 + dir.0, pos.1 + dir.1)) {
                let (mut px, mut py) = pos;
                while open.contains(&(px - dir.0, py - dir.1)) || walls.contains(&(px - dir.0, py - dir.1)) {
                    (px, py) = (px - dir.0, py - dir.1);
                }
                if open.contains(&(px, py)) {
                    p1_map.insert(state, State { pos: (px, py), dir });
                }
            }
        }
    }

    let p1 = instrs.iter().fold(init_state, |s, &i| go(&p1_map, s, i));
    println!("Part 1: {}", score(&p1));

    // Set up the map for P2 (cube style wraparound)
    // I couldn't think of a way to do this generically, so we do it manualy per our input layout, gluing the edges together.
    // Labels are just what I wrote on my piece of paper...

    let mut p2_map = common_map.clone();
    let mut glue_edges = |edge1, edge2, facing1, facing2| {
        for (&p1, &p2) in edge(50, edge1).iter().zip(edge(50, edge2).iter()) {
            if !walls.contains(&p1) && !walls.contains(&p2) {
                p2_map.insert(State { pos: p1, dir: facing1 }, State { pos: p2, dir: facing2 });
                p2_map.insert(State { pos: p2, dir: inv(facing2) }, State { pos: p1, dir: inv(facing1) });
            }
        }
    };

    glue_edges(((50, 0), DOWN), ((0, 149), UP), LEFT, RIGHT); // A => K
    glue_edges(((50, 0), RIGHT), ((0, 150), DOWN), UP, RIGHT); // B => L
    glue_edges(((100, 0), RIGHT), ((0, 199), RIGHT), UP, UP); // C => M
    glue_edges(((149, 0), DOWN), ((99, 149), UP), RIGHT, LEFT); // D => H
    glue_edges(((100, 49), RIGHT), ((99, 50), DOWN), DOWN, LEFT); // E => G
    glue_edges(((50, 50), DOWN), ((0, 100), RIGHT), LEFT, DOWN); // F => J
    glue_edges(((50, 149), RIGHT), ((49, 150), DOWN), DOWN, LEFT); // I => N

    let p2 = instrs.iter().fold(init_state, |s, &i| go(&p2_map, s, i));
    println!("Part 2: {}", score(&p2));
}
