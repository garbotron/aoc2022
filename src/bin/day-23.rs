use std::{fs, collections::{HashSet, HashMap}};

#[derive(Clone, Copy)]
enum Dir { N, S, W, E, NE, NW, SE, SW }

const ALL_DIRS: &[Dir] = &[Dir::N, Dir::S, Dir::W, Dir::E, Dir::NE, Dir::NW, Dir::SE, Dir::SW];
const PRIMARY_DIRS: &[Dir] = &[Dir::N, Dir::S, Dir::W, Dir::E];

#[derive(Clone)]
struct State {
    elves: HashSet<(i32, i32)>,
    primary_dir_idx: usize,
}

impl Dir {
    fn nearby(&self) -> [Dir; 3] {
        match *self {
            Self::N => [Self::N, Self::NE, Self::NW],
            Self::S => [Self::S, Self::SE, Self::SW],
            Self::W => [Self::W, Self::NW, Self::SW],
            Self::E => [Self::E, Self::NE, Self::SE],
            _ => panic!(),
        }
    }

    fn step(&self, (x, y): (i32, i32)) -> (i32, i32) {
        match *self {
            Self::N => (x, y - 1),
            Self::S => (x, y + 1),
            Self::W => (x - 1, y),
            Self::E => (x + 1, y),
            Self::NW => (x - 1, y - 1),
            Self::NE => (x + 1, y - 1),
            Self::SW => (x - 1, y + 1),
            Self::SE => (x + 1, y + 1),
        }
    }
}

impl State {
    fn dirs_to_check(&self) -> [Dir; 4] {
        [PRIMARY_DIRS[self.primary_dir_idx % 4],
         PRIMARY_DIRS[(self.primary_dir_idx + 1) % 4],
         PRIMARY_DIRS[(self.primary_dir_idx + 2) % 4],
         PRIMARY_DIRS[(self.primary_dir_idx + 3) % 4]]
    }

    fn score(&self) -> i32 {
        let min_x = self.elves.iter().map(|&(x, _)| x).min().unwrap();
        let max_x = self.elves.iter().map(|&(x, _)| x).max().unwrap();
        let min_y = self.elves.iter().map(|&(_, y)| y).min().unwrap();
        let max_y = self.elves.iter().map(|&(_, y)| y).max().unwrap();
        (max_x - min_x + 1) * (max_y - min_y + 1) - self.elves.len() as i32
    }

    fn perform_round(&self) -> Self {
        let mut proposed_moves = HashMap::new();
        let mut move_counts = HashMap::new();
        for &elf in self.elves.iter() {
            if ALL_DIRS.iter().all(|x| !self.elves.contains(&x.step(elf))) {
                proposed_moves.insert(elf, elf); // don't move
            } else {
                if let Some(&dir) = self.dirs_to_check().iter().find(|x| x.nearby().iter().all(|y| !self.elves.contains(&y.step(elf)))) {
                    let new_pos = dir.step(elf);
                    proposed_moves.insert(elf, new_pos);
                    move_counts.insert(new_pos, move_counts.get(&new_pos).unwrap_or(&0) + 1);
                } else {
                    proposed_moves.insert(elf, elf); // don't move
                }
            }
        }

        let mut elves = HashSet::new();
        for (&elf, &new_pos) in proposed_moves.iter() {
            if *move_counts.get(&new_pos).unwrap_or(&0) <= 1 {
                elves.insert(new_pos);
            } else {
                elves.insert(elf);
            }
        }
        State { elves, primary_dir_idx: (self.primary_dir_idx + 1) % 4 }
    }
}

fn main() {
    let mut initial_state = State { elves: HashSet::new(), primary_dir_idx: 0 };
    let input = fs::read_to_string("input/day-23.txt").unwrap();
    for (y, line) in input.lines().enumerate() {
        for (x, ch) in line.chars().enumerate() {
            if ch == '#' {
                initial_state.elves.insert((x as i32, y as i32));
            }
        }
    }

    let mut p1_state = initial_state.clone();
    for _ in 0..10 {
        p1_state = p1_state.perform_round();
    }
    println!("Part 1: {}", p1_state.score());

    let mut p2_state = initial_state.clone();
    let mut p2_prev = HashSet::new();
    for i in 0..i32::MAX {
        if p2_state.elves.eq(&p2_prev) {
            println!("Part 2: {}", i);
            break;
        }
        p2_prev = p2_state.elves.clone();
        p2_state = p2_state.perform_round();
    }
}
