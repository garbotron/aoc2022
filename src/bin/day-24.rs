use std::{fs, collections::{HashSet, HashMap}, cmp::{min, max}};

// Care of https://rosettacode.org/wiki/Least_common_multiple#Rust
fn gcd(a: i32, b: i32) -> i32 {
    match ((a, b), (a & 1, b & 1)) {
        ((x, y), _) if x == y => y,
        ((0, x), _) | ((x, 0), _) => x,
        ((x, y), (0, 1)) | ((y, x), (1, 0)) => gcd(x >> 1, y),
        ((x, y), (0, 0)) => gcd(x >> 1, y >> 1) << 1,
        ((x, y), (1, 1)) => {
            let (x, y) = (min(x, y), max(x, y));
            gcd((y - x) >> 1, x)
        }
        _ => unreachable!(),
    }
}

fn lcm(a: i32, b: i32) -> i32 {
    a * b / gcd(a, b)
}

fn main() {
    let input = fs::read_to_string("input/day-24.txt").unwrap();
    let height = (input.lines().count() - 2) as i32;
    let width = (input.lines().next().unwrap().len() - 2) as i32;

    // As we parse the input, fill in the array per cycle of which spots will have blizzards present.
    let cycle_len = lcm(height, width) as usize;
    let mut blizz_cycle = vec![HashSet::new(); cycle_len];
    for (y, line) in input.lines().enumerate() {
        for (x, ch) in line.chars().enumerate() {
            let blizzard_dir = match ch {
                '<' => Some((-1, 0)),
                '>' => Some((1, 0)),
                '^' => Some((0, -1)),
                'v' => Some((0, 1)),
                _ => None,
            };
            if let Some((dx, dy)) = blizzard_dir {
                let (mut x, mut y) = ((x - 1) as i32, (y - 1) as i32);
                for cycle in 0..cycle_len {
                    x = (x + dx + width) % width;
                    y = (y + dy + height) % height;
                    blizz_cycle[cycle].insert((x, y));
                }
            }
        }
    }

    let in_map = |x, y| {
        (x, y) == (0, -1) || (x, y) == (width - 1, height) || (x >= 0 && x < width && y >= 0 && y < height)
    };

    // Now start filling out the graph, with key=(position)+(point in the cycle), value=(# steps to get there).
    let step_count = |start_x, start_y, start_pt_in_cycle, end_x, end_y| {
        let mut overall_best = i32::MAX;
        let mut best_per_step: HashMap<(i32, i32, usize), i32> = HashMap::new();
        let mut to_explore = vec![(start_x, start_y, start_pt_in_cycle, 0)];
        while let Some((x, y, pt_in_cycle, cost_so_far)) = to_explore.pop() {
            if cost_so_far >= overall_best {
                continue;
            }
            if (x, y) == (end_x, end_y) {
                overall_best = cost_so_far;
                continue;
            }
            if let Some(&prev) = best_per_step.get(&(x, y, pt_in_cycle)) {
                if prev <= cost_so_far {
                    continue;
                }
            }
            best_per_step.insert((x, y, pt_in_cycle), cost_so_far);
            for (nx, ny) in [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1), (x, y)] {
                if in_map(x, y) && !blizz_cycle[pt_in_cycle].contains(&(nx, ny)) {
                    to_explore.push((nx, ny, (pt_in_cycle + 1) % cycle_len, cost_so_far + 1));
                }
            }
        }
        overall_best
    };

    let steps_there = step_count(0, -1, 0, width - 1, height);
    println!("Part 1: {steps_there}");

    let steps_back = step_count(width - 1, height, steps_there as usize % cycle_len, 0, -1);
    let and_again = step_count(0, -1, (steps_there + steps_back) as usize % cycle_len, width - 1, height);
    println!("Part 2: {}", steps_there + steps_back + and_again);

}
