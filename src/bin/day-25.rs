use std::fs;

fn from_snafu(s: &str) -> i64 {
    let mut ret = 0;
    let mut multiplier = 1;
    for ch in s.chars().rev() {
        let val = match ch {
            '2' => 2,
            '1' => 1,
            '0' => 0,
            '-' => -1,
            '=' => -2,
            _ => panic!(),
        };
        ret += multiplier * val;
        multiplier *= 5;
    }
    ret
}

fn to_snafu_part(num: i64, multiplier: i64) -> String {
    if multiplier == 0 {
        return String::new();
    }

    let mut max_next_val = 0;
    let mut next_multiplier = multiplier / 5;
    while next_multiplier > 0 {
        max_next_val += 2 * next_multiplier;
        next_multiplier /= 5;
    }

    if num > multiplier + max_next_val {
        String::from("2") + &to_snafu_part(num - 2 * multiplier, multiplier / 5)
    } else if num > max_next_val {
        String::from("1") + &to_snafu_part(num - multiplier, multiplier / 5)
    } else if num < -(multiplier + max_next_val) {
        String::from("=") + &to_snafu_part(num + 2 * multiplier, multiplier / 5)
    } else if num < -max_next_val {
        String::from("-") + &to_snafu_part(num + multiplier, multiplier / 5)
    } else {
        String::from("0") + &to_snafu_part(num, multiplier / 5)
    }
}

fn to_snafu(num: i64) -> String {
    to_snafu_part(num, 5i64.pow((num as f64).log(5.).floor() as u32))
}

fn main() {
    let input = fs::read_to_string("input/day-25.txt").unwrap();
    let sum: i64 = input.lines().map(from_snafu).sum();
    println!("{}", to_snafu(sum));
}
